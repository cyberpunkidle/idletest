﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class FightManager : MonoBehaviour {

	public Slider influenceSlider;
	public GameObject redWinUI;
	public GameObject blueWinUI;
	public GameObject redSquadParent;
	public GameObject blueSquadParent;
	public List<GameObject> MercPrefabs;
	private bool gamewon = false;

	public SquadInfo blueSquadInfo;
	public SquadInfo redSquadInfo;

	public List<Mercenary> redSquad = new List<Mercenary> ();
	public List<Mercenary> blueSquad = new List<Mercenary> ();

	private float attackWait = 2.5f;
	private float timer = 0;

	void Awake(){
		redSquad.Clear ();
		blueSquad.Clear ();
		ImportSquads ();

	}

	void Update(){
		if(Input.GetKeyDown(KeyCode.R)){
			ReloadScene ();
		}
		if (!gamewon && timer >= attackWait) {
			Attack ();
			timer = 0f;
		} else {
			timer += 0.1f;
		}
	}

	private void ImportSquads(){

		//get squad information from SOs
		//red squad
		for (int i = 0; i < redSquadInfo.mercList.Count; i++) {
			//get correct prefab
			for (int j = 0; j < MercPrefabs.Count; j++) {
				if (MercPrefabs [j].name == redSquadInfo.mercList [i]) {
					GameObject merc = GameObject.Instantiate (MercPrefabs [j], redSquadParent.transform) as GameObject;
					redSquad.Add (merc.GetComponentInChildren<Mercenary> ());
				}
			}
		}

		//blue squad
		for (int i = 0; i < blueSquadInfo.mercList.Count; i++) {
			//get correct prefab
			for (int j = 0; j < MercPrefabs.Count; j++) {
				if (MercPrefabs [j].name == blueSquadInfo.mercList [i]) {
					GameObject merc = GameObject.Instantiate (MercPrefabs [j], blueSquadParent.transform) as GameObject;
					blueSquad.Add (merc.GetComponentInChildren<Mercenary> ());
				}
			}
		}

	}

	public void RedWinGame(){
		gamewon = true;
		redWinUI.SetActive (true);
	}

	public void BlueWinGame(){
		gamewon = true;
		blueWinUI.SetActive (true);
	}

	public void CheckWinCondition(){
		if (0 == influenceSlider.value)
			BlueWinGame ();
		if (1 == influenceSlider.value)
			RedWinGame ();
	}

	private void Attack(){
		float redval = RedAttack ();
		float blueval = BlueAttack ();
		Debug.Log ("red: " + redval + " blue: " + blueval + " :: " + (redval - blueval));
		influenceSlider.value += (redval - blueval);
	}

	private float RedAttack(){
		int attackVal = 0;
		for (int i = 0; i < redSquad.Count-1; i++) {
			Mercenary merc = redSquad [i];
			attackVal += UnityEngine.Random.Range (merc.GetInfGainMin(), merc.GetInflGainMax ());
		}
		float temp = attackVal;
		temp /= 100;
		//Debug.Log ("Red Attack val: " + attackVal + "; " + temp);
		return temp;
	}

	private float BlueAttack(){
		int attackVal = 0;
		for (int i = 0; i < blueSquad.Count-1; i++) {
			Mercenary merc = blueSquad [i];
			attackVal += UnityEngine.Random.Range (merc.GetInfGainMin(), merc.GetInflGainMax ());
		}
		float temp = attackVal;
		temp /= 100;
		//Debug.Log ("Blue Attack val: " + attackVal + "; " + temp);
		return temp;
	}

	public void ReloadScene(){
		SceneManager.LoadScene(0);
	}
	public  void ManageMercs(){
		SceneManager.LoadScene (1);
	}
}
