﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEditor;

public class MercenarySquad : MonoBehaviour, IHasChanged {

	[SerializeField] Transform mercs;
	[SerializeField] Text mercText;

	public SquadInfo squadInfo;

	void Start(){
		HasChanged ();
	}

	#region IHasChanged implementation

	public void HasChanged ()
	{
		System.Text.StringBuilder builder = new System.Text.StringBuilder ();
		List<string> mNameList = new List<string> ();
		builder.Append (" - ");
		foreach (Transform slotTransform in mercs) {
			GameObject merc = slotTransform.GetComponent<MercenarySlot> ().merc;
			if (merc) {
				builder.Append (merc.name);
				builder.Append (" - ");
				mNameList.Add (slotTransform.GetComponentInChildren<Mercenary> ().mercName);
			}
		}
		mercText.text = builder.ToString ();
		squadInfo.mercList = mNameList;
		AssetDatabase.SaveAssets ();
	}
	#endregion

	public void DebugList(List<Mercenary> mList){
		System.Text.StringBuilder builder = new System.Text.StringBuilder ();
		for (int i = 0; i < mList.Count; i++) {
			builder.Append (mList [i].mercName);
			builder.Append (", ");
		}
		Debug.Log ("Mercs in party: " + builder.ToString ());
	}
}
	
namespace UnityEngine.EventSystems{
	public interface IHasChanged : IEventSystemHandler{
		void HasChanged ();
	}
}

public class SquadInfo : ScriptableObject {

	public List<string> mercList;

}