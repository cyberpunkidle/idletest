﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Mercenary : MonoBehaviour {
	public string mercName = "";
	public int influenceGainMin = 0;
	public int influenceGainMax = 20;


	public int GetInfGainMin(){
		return influenceGainMin;
	}

	public int GetInflGainMax(){
		return influenceGainMax;
	}

}
