﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class SpawnSOs {

	[MenuItem("Assets/Create/Squad Objects")]
	public static void CreateSquad(){
		SquadInfo sInfo = ScriptableObject.CreateInstance<SquadInfo> ();
		SquadInfo bInfo = ScriptableObject.CreateInstance<SquadInfo> ();

		AssetDatabase.CreateAsset (sInfo, "Assets/RedSquad.asset");
		AssetDatabase.CreateAsset (bInfo, "Assets/BlueSquad.asset");
		AssetDatabase.SaveAssets();

		EditorUtility.FocusProjectWindow ();
		Selection.activeObject = bInfo;
	}

}
